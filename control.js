//-----------------------------------------------
// Proxy to access current tab recorder instance
// ----------------------------------------------
function RecorderProxy() {
    this.active = null;
}

RecorderProxy.prototype.start = function (url, name, application) {
    chrome.tabs.getSelected(null, function (tab) {
        chrome.runtime.sendMessage({ action: "start", recorded_tab: tab.id, 
        start_url: url, name: name, application: application,});
    });
}

RecorderProxy.prototype.stop = function () {
    chrome.runtime.sendMessage({ action: "stop" });
}

RecorderProxy.prototype.open = function (url, callback) {
    chrome.tabs.getSelected(null, function (tab) {
        chrome.tabs.sendMessage(tab.id, { action: "open", 'url': url }, callback);
    });
}


//-----------------------------------------------
// UI
//----------------------------------------------
function RecorderUI() {
    this.recorder = new RecorderProxy();

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        console.log(request)
        if (request.action == "user_state") {
            if (request.userauth) {
                ui.set_connected();
                chrome.runtime.sendMessage({ action: "get_status" }, function (response) {
                    console.log(response);
                    if (response.active) {
                        ui.set_started();
                    } else {
                        if (!response.empty) {
                            ui.set_stopped();
                        }
                        chrome.tabs.getSelected(null, function (tab) {
                            document.forms[0].elements["url"].value = tab.url;
                        });
                    }
                });
            } else {
                ui.set_disconnected();
            }
        }else if (request.action == "user_applications") {
            var applications = request.applications;
            var applicationsSelector =  document.getElementById("application");
            applicationsSelector.innerHTML = '';
            for(var i =0; i< applications.length; i++){
                applicationsSelector.innerHTML += '<option value="'+ applications[i].id +'">'+ applications[i].name +'</option>';
            }
        }
    });
    chrome.runtime.sendMessage({ action: "user_auth" });
}

RecorderUI.prototype.start = function () {
    var url = document.forms[0].elements["url"].value;
    var name = document.forms[0].elements["name"].value;
    var application = document.forms[0].elements["application"].value;
    if (url == "" || name == "" || application == "") {
    var e = document.getElementById("rerror");
    e.className = e.className.replace(/ hide|hide/ig, "");
    return false;
    }
    if ((url.indexOf("http://") == -1) && (url.indexOf("https://"))) {
        url = "http://" + url;
    }
    ui.set_started()
    ui.recorder.start(url, name, application);

    return false;
}

RecorderUI.prototype.set_connected = function () {
    var e = document.getElementById("blogin");
    e.className += " hide";
    e = document.getElementById("url");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("test-name");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("application-name");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("bstop");
    e.className += " hide";
    e = document.getElementById("bgo");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("ldescription");
    e.className += " hide";
    e = document.getElementById("rdescription");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("rerror");
    e.className += " hide";
    e = document.getElementById("tmessage");
    e.className += " hide";
}

RecorderUI.prototype.set_disconnected = function () {
    var e = document.getElementById("blogin");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("url");
    e.className += " hide";
    e = document.getElementById("test-name");
    e.className += " hide";
    e = document.getElementById("application-name");
    e.className += " hide";
    e = document.getElementById("bstop");
    e.className += " hide";
    e = document.getElementById("bgo");
    e.className += " hide";
    e = document.getElementById("ldescription");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("rdescription");
    e.className += " hide";
    e = document.getElementById("tmessage");
    e.className += " hide";
}

RecorderUI.prototype.set_started = function () {
    var e = document.getElementById("bstop");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e.onclick = ui.stop;
    e.value = "Stop Recording";
    e = document.getElementById("bgo");
    e.className += " hide";
    e = document.getElementById("tmessage");
    e.className += " hide";
    e = document.getElementById("recording");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("turl");
    e.disabled = true;
    e = document.getElementById("rdescription");
    e.className += " hide";
    e = document.getElementById("rerror");
    e.className += " hide";
    e = document.getElementById("test-name");
    e.className += " hide";
    e.value = "";
    e = document.getElementById("application-name");
    e.className += " hide";

    chrome.browserAction.setBadgeText({
        "text": "REC"
    });
    chrome.browserAction.setBadgeBackgroundColor({
        "color": "#c53929"
    })
}

RecorderUI.prototype.stop = function () {
    ui.set_stopped();
    ui.recorder.stop();
    var e = document.getElementById("tmessage");
    e.className = e.className.replace(/ hide|hide/ig, "");
    return false;
}

RecorderUI.prototype.set_stopped = function () {
    var e = document.getElementById("bstop");
    e.className += " hide";
    e = document.getElementById("bgo");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("recording");
    e.className += " hide";
    e = document.getElementById("blogin");
    e.className += " hide";
    e = document.getElementById("turl");
    e.disabled = false;
    e = document.getElementById("test-name");
    e.className = e.className.replace(/ hide|hide/ig, "");
    e = document.getElementById("application-name");
    e.className = e.className.replace(/ hide|hide/ig, "");

    chrome.browserAction.setBadgeText({
        "text": ""
    });
    chrome.browserAction.setBadgeBackgroundColor({
        "color": "#ffffff"
    })
}

RecorderUI.prototype.setBtnGoState = function () {
    chrome.tabs.getSelected(null, function (tab) {
        if (/(chrome|chrome\-extension)\:/.test(tab.url)) {
            document.querySelector("input#bgo").className += " disabled";
            document.querySelector("input#bgo").disabled = true;
        }
    });
    document.querySelector("input#turl").addEventListener("input", function () {
        var bgoStyle = document.querySelector("input#bgo");
        if (!/(chrome|chrome\-extension)\:/.test(this.value)) {
            bgoStyle.className = bgoStyle.className.replace(/ disabled|disabled/ig, "");
            bgoStyle.disabled = false;
        }
    });
}

var ui;

// bind events to ui elements
window.onload = function () {
    document.querySelector('input#blogin').onclick = function () {
        window.open("http://localhost:8080/arqgen/login", '_blank');
        return false;
    };
    document.querySelector('input#bgo').onclick = function () { ui.start(); return false; };
    document.querySelector('input#bstop').onclick = function () { ui.stop(); return false; };
    ui = new RecorderUI();
    ui.setBtnGoState();
}