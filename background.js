var testcase_items = new Array();
var name ='';
var application=0;
var active = false;
var empty = true;
var tab_id = null;
var userState = false;
console.log('init');

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  console.log(request);
  if (request.action == "append") {
    testcase_items[testcase_items.length] = request.obj;
    empty = false;
    sendResponse({});
  }
  if (request.action == "poke") {
    testcase_items[testcase_items.length - 1] = request.obj;
    sendResponse({});
  }
  if (request.action == "get_status") {
    sendResponse({ 'active': active, 'empty': empty });
  }
  if (request.action == "start") {
    if (!active) {
      active = true;
      empty = true;
      testcase_items = new Array();
      tab_id = request.recorded_tab;
      name = request.name;
      application = request.application;

      chrome.tabs.update(tab_id, { url: request.start_url }, function (tab) {
        console.log("You are now recording your test sequence.");
        chrome.tabs.sendMessage(tab_id, { action: "open", 'url': request.start_url });
        sendResponse({ start: true });
      });
    }
  }
  if (request.action == "stop") {
    active = false;
    chrome.tabs.sendMessage(tab_id, { action: "stop" });
    sendResponse({});
    sendTestCase();
  }
  if (request.action == "get_items") {
    sendResponse({ 'items': testcase_items });
  }
  if (request.action == "user_auth") {
    userState = false;
    var promise = new Promise(function (resolve, reject) {
      checkUser(resolve, reject);
    });

    promise.then(function(result) {
      if(userState){
        initUserApplications();
      }
    }, function(err) {
      console.log(err);
    });
  }
});


initUserApplications = function () {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4) {
      if (this.status == 200) {
        var applications = JSON.parse(xmlhttp.response);
        chrome.runtime.sendMessage({ 'action':'user_applications', 'applications': applications});
        chrome.runtime.sendMessage({ 'action':'user_state', 'userauth': userState});
      }
    }
  };
  xmlhttp.open("GET", "http://localhost:8080/arqgen/api/v1/test/record/application/list");
  xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlhttp.send(JSON.stringify({ applicationId: 1 , description:"test from extension", tests: testcase_items}));
}

sendTestCase = function () {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "http://localhost:8080/arqgen/api/v1/test/record/test/save");
  xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlhttp.send(JSON.stringify({ applicationId: application , name:name, tests: testcase_items}));
  var result = xmlhttp.responseText;
}

checkUser = function (resolve, reject) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4) {
      if (this.status == 200) {
        userState = JSON.parse(xmlhttp.response);
        resolve("get worked!");
      }
      else {
        reject(Error("It broke"));
      }
    }
  };
  xmlhttp.open("GET", "http://localhost:8080/arqgen/api/v1/test/record/test/auth");
  xmlhttp.send();
}